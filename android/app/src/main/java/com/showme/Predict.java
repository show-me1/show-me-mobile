package com.showme;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

public class PredictModule extends ReactContextBaseJavaModule {
  public PredictModule(ReactApplicationContext reactContext) {
    super(reactContext);
  }

  @Override
  public String getName() {
    return "Predict";
  }

  @ReactMethod
  public void predict(Image image, Promise promise) {
    try {
        promise.resolve();
    } catch (Exception exception) {
        promise.reject();
    }
  }
}