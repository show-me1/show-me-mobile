import { StackNavigationProp } from '@react-navigation/stack';
import {createStackNavigator} from '@react-navigation/stack';

type RootStackParamList = {
    Home: undefined;
    Capture: undefined;
    Search: [] | undefined;
};

export const RootStack = createStackNavigator<RootStackParamList>();

export interface ScreenProps {
    navigation: StackNavigationProp<RootStackParamList>
}

