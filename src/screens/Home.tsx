import * as React from "react";
import { View, Text, Button} from 'react-native';
import {ScreenProps} from "./ScreenProps";

const Home: React.FC<ScreenProps> = (props: ScreenProps) => {
    return (
        <View>
            <Text>This is the home screen of the app</Text>
            <Button
                onPress={() =>
                props.navigation.navigate('Capture')
                }
                title="Capture"
            />
            <Button
                onPress={() =>
                props.navigation.navigate('Search')
                }
                title="Search"
            />
        </View>
    );
}

export {Home};