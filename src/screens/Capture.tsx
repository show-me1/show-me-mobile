import * as React from "react";
import {View, Text, Button} from 'react-native';
import {ScreenProps} from "./ScreenProps";

const Capture: React.FC<ScreenProps> = (props: ScreenProps) => {
    return (
        <View>
            <Text>Capture page</Text>
            <Button
                onPress={() =>
                props.navigation.navigate('Home')
                }
                title="Return"
            />
        </View>
    );
}

export {Capture};