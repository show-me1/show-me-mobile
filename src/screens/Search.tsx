import * as React from "react";
import {View, Text, Button} from 'react-native';
import {ScreenProps} from "./ScreenProps";

const Search: React.FC<ScreenProps> = (props: ScreenProps) => {
    return (
        <View>
            <Text>Search page</Text>
            <Button
                onPress={() =>
                props.navigation.navigate('Home')
                }
                title="Return"
            />
        </View>
    );
}

export {Search};