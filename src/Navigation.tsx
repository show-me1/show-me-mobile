import * as React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {RootStack} from "./screens/ScreenProps";

import {Home} from './screens/Home';
import {Search} from './screens/Search';
import {Capture} from './screens/Capture';

const Stack = RootStack;

const Navigation = () => {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={Home}
          />
          <Stack.Screen 
            name="Capture" 
            component={Capture} 
          />
          <Stack.Screen 
            name="Search" 
            component={Search} 
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }

  export default Navigation;